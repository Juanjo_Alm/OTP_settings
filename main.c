#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAX_STRING_SIZE	2048

#define 	CRC16 						0x8005
#define		CRC_START_16				0x0000

#define 	OTP_start_memory			0x1FFF7000
#define		OTP_block_settings_size		14 //bytes



typedef struct{
	uint16_t 	crc16;
	uint32_t 	addrPointer;
	uint8_t 	driverMotor;
	uint8_t 	efectoHall;
	uint8_t 	NFC;
	uint8_t 	radio;
	uint8_t 	sensorTemp;
	uint8_t 	flash;
	uint8_t 	RAM;
	uint8_t 	corrienteMAX
}estructura_OTP;

void introduceSettings(estructura_OTP* settings, char* input){
	
	settings->crc16 = 0x0000;
	settings->addrPointer = OTP_start_memory + OTP_block_settings_size - 1;
	printf("Se va a usar el driver motor: viejo(Pulsa 0) / nuevo(Pulsa 1)\n");
	fgets( input, MAX_STRING_SIZE-1, stdin );
	settings->driverMotor = input[0] - '0';
	settings->efectoHall = 0;
	settings->flash = 0;
	settings->NFC = 0;
	settings->radio = 0;
	settings->RAM = 0;
	settings->sensorTemp = 0;
}

uint16_t gen_crc16(const uint8_t *data, uint16_t size)
{
    uint16_t out = 0;
    int bits_read = 0, bit_flag;

    /* Sanity check: */
    if(data == NULL)
        return 0;

    while(size > 0)
    {
        bit_flag = out >> 15;

        /* Get next bit: */
        out <<= 1;
        out |= (*data >> bits_read) & 1; // item a) work from the least significant bits

        /* Increment bit counter: */
        bits_read++;
        if(bits_read > 7)
        {
            bits_read = 0;
            data++;
            size--;
        }

        /* Cycle check: */
        if(bit_flag)
            out ^= CRC16;

    }

    // item b) "push out" the last 16 bits
    int i;
    for (i = 0; i < 16; ++i) {
        bit_flag = out >> 15;
        out <<= 1;
        if(bit_flag)
            out ^= CRC16;
    }

    // item c) reverse the bits
    uint16_t crc = 0;
    i = 0x8000;
    int j = 0x0001;
    for (; i != 0; i >>=1, j <<= 1) {
        if (i & out) crc |= j;
    }

    return crc;
}

void formato_bin_little_endian(estructura_OTP* settings){
	
	FILE *fp;
	int i; 	
	
	fp = fopen("settings.bin", "w");
    uint8_t buff[OTP_block_settings_size];
    
    //CRC16
    printf("CRC16: 0x%04x\n", settings->crc16);
    buff[0] = settings->crc16 >> 8;
    buff[1] = settings->crc16 & 0xff;
    //addrPointer
    printf("addrPointer: 0x%08x\n", settings->addrPointer);
    buff[2] = settings->addrPointer >> 24;
    buff[3] = (settings->addrPointer & 0xff0000) >> 16;
    buff[4] = (settings->addrPointer & 0xff00) >> 8;
    buff[5] = settings->addrPointer & 0xff;
    //driverMotor
    buff[6] = settings->driverMotor & 0xff;
    //efectoHall
    buff[7] = settings->efectoHall & 0xff;
    //NFC
    buff[8] = settings->NFC & 0xff;
    //radio
    buff[9] = settings->radio & 0xff;
    //sensorTemp
    buff[10] = settings->sensorTemp & 0xff;
    //flash
    buff[11] = settings->flash & 0xff;
    //RAM
    buff[12] = settings->RAM & 0xff;
    //corrienteMAX
    buff[13] = settings->corrienteMAX & 0xff;
    
    
    fwrite(buff,sizeof(buff),1,fp);   
	
	fclose(fp);   
}

void llenadoBufferSettingsCRC(estructura_OTP* settings, uint8_t* buffer){
	//addrPointer
    buffer[0] = settings->addrPointer >> 24;
    buffer[1] = (settings->addrPointer & 0xff0000) >> 16;
    buffer[2] = (settings->addrPointer & 0xff00) >> 8;
    buffer[3] = settings->addrPointer & 0xff;
    //driverMotor
    buffer[4] = settings->driverMotor & 0xff;
    //efectoHall
    buffer[5] = settings->efectoHall & 0xff;
    //NFC
    buffer[6] = settings->NFC & 0xff;
    //radio
    buffer[7] = settings->radio & 0xff;
    //sensorTemp
    buffer[8] = settings->sensorTemp & 0xff;
    //flash
    buffer[9] = settings->flash & 0xff;
    //RAM
    buffer[10] = settings->RAM & 0xff;
    //corrienteMAX
    buffer[11] = settings->corrienteMAX & 0xff;
}

/*
 * int main( int argc, char *argv[] );
 *
 * The function main() is the entry point of the example program which
 * calculates several CRC values from the contents of files, or data from
 * stdin.
 */

int main( int argc, char *argv[] ) {

	char input_string[MAX_STRING_SIZE];	
	estructura_OTP estructuraOTP;
	uint8_t buffer_CRC[OTP_block_settings_size - 2];

	introduceSettings(&estructuraOTP, &input_string);
	llenadoBufferSettingsCRC(&estructuraOTP, &buffer_CRC);
	estructuraOTP.crc16 = gen_crc16(&buffer_CRC, sizeof(buffer_CRC));
	formato_bin_little_endian(&estructuraOTP);
	
	//Prueba de CRC16 -> Resultado bb3d
	/*uint8_t ui_VectorSample[] = {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39};
	estructuraOTP.crc16 = gen_crc16(ui_VectorSample , 9);
	printf("CRC16: 0x%04x\n", estructuraOTP.crc16);	*/	
	
	return 0;

}  /* main (tstcrc.c) */
